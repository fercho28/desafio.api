﻿using BCP.TIPOCAMBIO.APIDEMO.DAL;
using BCP.TIPOCAMBIO.APIDEMO.DTO;
using BCP.TIPOCAMBIO.APIDEMO.MODELO;
using BCP.TIPOCAMBIO.APIDEMO.VALIDACION;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCP.TIPOCAMBIO.APIDEMO.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class tipocambioController : ControllerBase
    {


        MensajeEstatusDTOcs _mensResp = new MensajeEstatusDTOcs();
        List<tipoCambioBE> listaTC = new List<tipoCambioBE>();
        ConexionDAL aa = new ConexionDAL();
        Util validar = new Util();
        private IEnumerable<string> pvalor1;
        private IEnumerable<string> pvalor2;

        [HttpPut]
        [Route("update")]
        public ActionResult PutCambioUSD(tipoCambioBE tipo)
        {

            try
            {

                if (tipo.moneda.Length == 3 && !(string.IsNullOrEmpty(tipo.moneda.TrimEnd())) && tipo.tipoCambioUSD.TrimEnd() != "")
                {
                    _mensResp.descripcionRespuesta = aa.updateTC(tipo.moneda, tipo.tipoCambioUSD);
                    _mensResp.codigoRespuesta = "EXITO";
                }
                else
                {
                    _mensResp.codigoRespuesta = "ERROR101";
                    _mensResp.descripcionRespuesta = ConstantesBE.MS102;
                }
            }
            catch (Exception ex)
            {
                _mensResp.codigoRespuesta = "ERROR101";
                _mensResp.descripcionRespuesta = ConstantesBE.ERROR101 + " " + ex.Message;
                return BadRequest(_mensResp);
            }

            return Ok(_mensResp);
        }



        [HttpGet()]
        [Route("consultar-tipocambio")]
        public ActionResult GetTipoCambio(double monto, string monedaOrigen, string monedaDestino)
        {

            TipoCambioDTO _tipoCambioDTO = new TipoCambioDTO();

            double tipoCambioMontoFinal = 0;
            double tipoCambioFinal = 0;
            string flagObservado = string.Empty;
            double tcOrigen = 0;
            double tcDestino = 0;

            try
            {
                _mensResp = validar.validarCampos2(monto, monedaOrigen, monedaDestino);

                if (_mensResp.codigoRespuesta == "EXITO")
                {
                    listaTC = aa.ConsultarList();
                    if (listaTC.Count > 0)
                    {
                        // Moneda Origen
                        pvalor1 = from cust in listaTC
                                  where cust.moneda == monedaOrigen
                                  select cust.tipoCambioUSD;


                        pvalor2 = from cust in listaTC
                                  where cust.moneda == monedaDestino
                                  select cust.tipoCambioUSD;
                        // Moneda Destino
                        if (pvalor1.ToList().Count() == 0 || pvalor1.ToList().Count() == 0)
                        {

                            _mensResp.codigoRespuesta = "ERROR";
                            _mensResp.descripcionRespuesta = ConstantesBE.SINTIPOCAMBIO;
                            return new JsonResult(_mensResp);
                        }

                        else
                        {

                            tcOrigen = double.Parse(pvalor1.FirstOrDefault());
                            tcDestino = double.Parse(pvalor2.FirstOrDefault());
                            tipoCambioFinal = tcOrigen / tcDestino;
                            tipoCambioMontoFinal = monto / tipoCambioFinal;

                            _tipoCambioDTO.monto = monto;
                            _tipoCambioDTO.montoTipoCambio = tipoCambioMontoFinal;
                            _tipoCambioDTO.monedaOrigen = monedaOrigen;
                            _tipoCambioDTO.monedaDestino = monedaDestino;
                            _tipoCambioDTO.tipoCambio = tipoCambioFinal;

                            return new JsonResult(_tipoCambioDTO);
                        }

                    }

                    else
                    {
                        _mensResp.codigoRespuesta = "ERROR";
                        _mensResp.descripcionRespuesta = ConstantesBE.SINTIPOCAMBIO;
                        return new JsonResult(_mensResp);
                    }
                }

                else
                {
                    _mensResp.codigoRespuesta = "OBSERVADO";
                    return new JsonResult(_mensResp);
                }

            }
            catch (Exception ex)
            {
                _mensResp.codigoRespuesta = "ERROR";
                _mensResp.descripcionRespuesta = ConstantesBE.ERROR100 + " " + ex.Message;
                return new JsonResult(_mensResp);
            }


        }



        [HttpGet()]
        [Route("consultar-listamoneda")]

        public JsonResult GetLista()
        {

            List<tipoCambioBE> lista = new List<tipoCambioBE>();
            try
            {
                lista = aa.ConsultarList();

                if (lista.Count() < 1)
                {
                    _mensResp.codigoRespuesta = "EXITO";
                    _mensResp.descripcionRespuesta = ConstantesBE.SINREGISTRO;
                    return new JsonResult(_mensResp);
                }
                else
                {
                    return new JsonResult(lista);
                }
               
            }
            catch (Exception ex)
            {
                _mensResp.codigoRespuesta = "ERROR";
                _mensResp.descripcionRespuesta = ex.Message;
                return new JsonResult(_mensResp);
            }

        }





        [HttpPost]
        [Route("insertar")]
        public ActionResult PostInsertar(tipoCambioBE dep)
        {
            try
            {
                _mensResp = validar.validarCampos1(dep.moneda, dep.tipoCambioUSD);

                var observado = aa.insertar(dep.moneda, dep.tipoCambioUSD);

                if (observado == "EXITO")
                {
                    _mensResp.codigoRespuesta = observado;
                    _mensResp.descripcionRespuesta = ConstantesBE.EXITO;
                }
                else
                {
                    _mensResp.codigoRespuesta = "ERROR";
                    _mensResp.descripcionRespuesta = ConstantesBE.ERROR102;
                }

            }
            catch (Exception ex)
            {
                _mensResp.codigoRespuesta = "ERROR102";
                _mensResp.descripcionRespuesta = ConstantesBE.ERROR102 + " " + ex.Message;
                return BadRequest(_mensResp);
            }

            return Ok(_mensResp);
        }





    }

}

