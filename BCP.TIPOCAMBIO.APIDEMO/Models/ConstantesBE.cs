using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCP.TIPOCAMBIO.APIDEMO.MODELO
{
  public static class ConstantesBE
  {


    public const string ERROR100 = "ERROR INESPERADO, CONTACTAR CON EL ADMINISTRADOR: ";
    public const string MS101 = "NO ES PERMITIDO CAMPOS VACIOS";
    public const string MS102 = "FORMATO y/o LONGITUD NO PERMITIDO EN EL CAMPO";
    public const string MS103 = "FORMATO y/o LONGITUD NO PERMITIDO EN EL CAMPO";
    public const string ERROR101 = "ERROR OCURRIDO EN EL UPDATE DEL REGISTRO";
    public const string ERROR102 = "ERROR OCURRIDO EN EL INSERT DEL REGISTRO";



    public const string EXITO = "SE REALIZÓ LA OPERACIÓN CON EXITO";
    public const string SINREGISTRO = "NO HAY REGISTRO EN LA BASE DE DATOS";
    public const string SINTIPOCAMBIO = "NO HAY TIPO DE CAMBIO EN LA BASE DE DATOS";
  }
}
