using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCP.TIPOCAMBIO.APIDEMO.DTO
{
  public class MensajeEstatusDTOcs
  {
    public string codigoRespuesta { get; set; }
    public string descripcionRespuesta { get; set; }
  }
}
