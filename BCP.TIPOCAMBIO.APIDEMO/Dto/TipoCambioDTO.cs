using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BCP.TIPOCAMBIO.APIDEMO.DTO
{
  public class TipoCambioDTO
  {


    public double monto { get; set; }
    public double montoTipoCambio { get; set; }
    public string monedaOrigen { get; set; }
   
    public string monedaDestino { get; set; }
    public double tipoCambio { get; set; }


    }

  }

