using BCP.TIPOCAMBIO.APIDEMO.MODELO;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

 
namespace BCP.TIPOCAMBIO.APIDEMO.DAL
{
  public class ConexionDAL
  {

    List<tipoCambioBE> lista = new List<tipoCambioBE>();
    public List<tipoCambioBE> ConsultarList()
    {
      var conex = new SqliteConnectionStringBuilder();

      conex.DataSource = ".\\databaseTC.db";

      string query = @"
                   select moneda, tipoCambioUSD  from tipoCambio";

      using (var conection = new SqliteConnection(conex.ConnectionString))
      {
        conection.Open();
        var select = conection.CreateCommand();
        select.CommandText = query;

        using (var reader = select.ExecuteReader())

        {

          if (reader != null)
          {
            lista = new List<tipoCambioBE>();

            int pos1_Id = reader.GetOrdinal("moneda");
            int pos2_Des = reader.GetOrdinal("tipoCambioUSD");

            tipoCambioBE obTipoCambioBE = null;
            while (reader.Read())
            {

              obTipoCambioBE = new tipoCambioBE();
              obTipoCambioBE.moneda = reader.IsDBNull(pos1_Id) ? "" : reader.GetString(pos1_Id);
              obTipoCambioBE.tipoCambioUSD = reader.IsDBNull(pos2_Des) ? "" : reader.GetString(pos2_Des);

              lista.Add(obTipoCambioBE);
            }
            reader.Close();
          }
          reader.Close();

        }

        return lista;
      }

    }





    public string updateTC(string moneda, string tipoCambioUSD)
    {

      string flagError = ConstantesBE.ERROR101; 
      string query = @"
                    update tipoCambio set 
                    tipoCambioUSD = '" + tipoCambioUSD + @"'
                    where moneda = '" + moneda + @"' 
                    ";

      var conex = new SqliteConnectionStringBuilder();

      conex.DataSource = ".\\databaseTC.db";

      using (var conection = new SqliteConnection(conex.ConnectionString))
      {
        conection.Open();

        using (var transaction = conection.BeginTransaction())
        {
          var actualizar = conection.CreateCommand();
          actualizar.CommandText = query;
          actualizar.ExecuteNonQuery();
          transaction.Commit();
          flagError = ConstantesBE.EXITO;
        }
      }
      return flagError;
    }





    public string  insertar(string moneda, string tipoCambio )
    {
      string flagError = ConstantesBE.ERROR102;

      string query = @"
                    insert into tipoCambio (moneda, tipoCambioUSD) values 
                    ('" + moneda + "', '" + tipoCambio + "')";

      var conex = new SqliteConnectionStringBuilder();

      conex.DataSource = ".\\databaseTC.db";


      using (var conection = new SqliteConnection(conex.ConnectionString))
      {
        conection.Open();

        using (var transaction = conection.BeginTransaction())
        {
          var insertar = conection.CreateCommand();
          insertar.CommandText = query;
          insertar.ExecuteNonQuery();
          transaction.Commit();
          flagError = "EXITO";

        }

      }


      return flagError;

    }




  }
}
