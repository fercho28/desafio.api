using BCP.TIPOCAMBIO.APIDEMO.DTO;
using BCP.TIPOCAMBIO.APIDEMO.MODELO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace BCP.TIPOCAMBIO.APIDEMO.VALIDACION
{
  public class Util
  {

    MensajeEstatusDTOcs _mensResp = new MensajeEstatusDTOcs();

    public MensajeEstatusDTOcs validarCampos1(string moneda, string tipocambio)
    {
        if (string.IsNullOrEmpty(moneda.TrimEnd()) || string.IsNullOrEmpty(tipocambio.TrimEnd()))
        {
          _mensResp.codigoRespuesta = "MS101";
          _mensResp.descripcionRespuesta = ConstantesBE.MS101;
        }
        else if (!(moneda.TrimStart().Length == 3))
        {
          _mensResp.codigoRespuesta = "MS102";
          _mensResp.descripcionRespuesta = ConstantesBE.MS102;
        }
        else if (double.Parse(tipocambio) < 0)
        {
        _mensResp.codigoRespuesta = "MS103";
        _mensResp.descripcionRespuesta = ConstantesBE.MS103;
      }
       
        else
        {
          _mensResp.codigoRespuesta = "EXITO";
        }
        return _mensResp;



      }


    public MensajeEstatusDTOcs validarCampos2( double monto, string monedaOrigen, string monedaDestino)
    {

      if (string.IsNullOrEmpty(monedaOrigen) || string.IsNullOrEmpty(monedaDestino)    )
      {
        _mensResp.codigoRespuesta = "MS101";
        _mensResp.descripcionRespuesta = ConstantesBE.MS101;
      }
      else if (!(monedaOrigen.TrimStart().Length == 3) || !(monedaDestino.TrimStart().Length == 3))
      {
        _mensResp.codigoRespuesta = "MS102";
        _mensResp.descripcionRespuesta = ConstantesBE.MS102;
      }
      else if (monto < 0)
      {
        _mensResp.codigoRespuesta = "MS103";
        _mensResp.descripcionRespuesta = ConstantesBE.MS103;
      }

      else
      {
        _mensResp.codigoRespuesta = "EXITO";
      }


      return _mensResp;



    }



  }
}
